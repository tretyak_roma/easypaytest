﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Easypay.WebUI.Models
{
    public class AddSumModel
    {
        public int ProviderId { get; set; }

        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Сумма")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Недопустимое значение")]
        public decimal Sum { get; set; }

        public DateTime DatePost { get; set; }
    }
}