﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Easypay.WebUI.Models
{
    public class ListViewModel
    {
        [Display(Name = "№")]
        public int ServiceIdProvider { get; set; }

        [Display(Name = "Последнее обновление")]
        public DateTime DatePostProvider { get; set; }

        [Display(Name = "Имя")]
        public string NameProvider { get; set; }

        [Display(Name = "Статус")]
        public bool? ActiveProvider { get; set; }

        [Display(Name = "Сумма")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Недопустимое значение")]
        public decimal Sum { get; set; }

        [Display(Name = "Последнее обновление cуммы")]
        public DateTime SumDateTime { get; set; }
    }
}