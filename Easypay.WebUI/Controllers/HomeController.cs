﻿using System;
using System.Linq;
using System.Web.Mvc;
using Easypay.Domain.Abstract;
using Easypay.Domain.Conctere;
using Easypay.WebUI.Models;

namespace Easypay.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private IRepository repository; 

        public HomeController(IRepository repo)
        {
            repository = repo;
        }

        public ActionResult Index()
        {
            var query = (from t1 in repository.ProviderBalances
                join t2 in (
                    (from tt in repository.BalanceSums
                        join tr in (
                            (from tbServicesBalancesSums in repository.BalanceSums
                                group tbServicesBalancesSums by new
                                {
                                    tbServicesBalancesSums.service_id
                                }
                                into g
                                select new
                                {
                                    g.Key.service_id,
                                    datepost = g.Max(p => p.date_post)
                                }))
                            on new {tt.service_id, datepost = tt.date_post}
                            equals new {tr.service_id, tr.datepost}
                        select new
                        {
                            tt.sum,
                            tt.date_post,
                            tr.service_id
                        })) on new {service_id = t1.ServiceId} equals new {service_id = Convert.ToInt32(t2.service_id)}
                select new ListViewModel
                {
                    ServiceIdProvider = t1.ServiceId,
                    //t1.DatePost,
                    NameProvider = t1.Name,
                    ActiveProvider = t1.Active,
                    SumDateTime = t2.date_post,
                    Sum = t2.sum
                }).OrderByDescending(x=> x.SumDateTime);

            return View(query);
        }

        public ActionResult ViewAddSum(int serviceId)
        {
            try
            {
                var balanceSums = (from balance in repository.BalanceSums
                                   where balance.service_id == serviceId
                                   orderby balance.date_post descending
                                   select balance).First();


                var providerName = from provider in repository.ProviderBalances
                                   where provider.ServiceId == serviceId
                                   select provider.Name;

                var viewModel = new AddSumModel
                {
                    Name = providerName.FirstOrDefault(),
                    Sum = balanceSums == null ? 0 : balanceSums.sum,
                    ProviderId = serviceId

                };
                return PartialView("_ModalSum", viewModel);
            }
            catch (Exception ex)
            {
                // Save in log file.
                ViewBag.Error = ex.ToString();
            }

            return PartialView("_ModalSum");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSum(AddSumModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var newBalanceSum = new tb_services_balances_sums
                {
                    date_post = DateTime.Now,
                    service_id = viewModel.ProviderId,
                    sum = viewModel.Sum
                };

                repository.SaveItem(newBalanceSum);

                return RedirectToAction("Index");
            }
            return PartialView("_ModalSum", viewModel);
            
        }
    }
}