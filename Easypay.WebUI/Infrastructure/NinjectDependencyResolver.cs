﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Easypay.Domain.Conctere;

using Moq;
using Ninject;
using Easypay.Domain.Abstract;

namespace Easypay.WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
        private void AddBindings()
        {
            //var mock = new Mock<IRepository>();
            // mock.Setup(m => m.ProviderBalances).Returns(new List<ProviderBalance>
            // {
            //      new ProviderBalance { Name = "name1", Active = true, DatePost = DateTime.Now},
            //      new ProviderBalance { Name = "name2", Active = true, DatePost = DateTime.Now},
            //      new ProviderBalance { Name = "name3", Active = true, DatePost = DateTime.Now},
            // });

            // // Привязки.

            //kernel.Bind<IRepository>().ToConstant(mock.Object);

            kernel.Bind<IRepository>().To<EfRepository>();
        }

        }
    }
