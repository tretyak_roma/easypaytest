﻿using System;
using System.Collections.Generic;
using Easypay.Domain.Abstract;

namespace Easypay.Domain.Conctere
{
    public class EfRepository : IRepository
    {

        EasypayDBEntities context = new EasypayDBEntities();

        public IEnumerable<tb_services_balances> ProviderBalances
        {
            get { return context.tb_services_balances; }
        }

        public IEnumerable<tb_services_balances_sums> BalanceSums
        {
            get { return context.tb_services_balances_sums; }

        }

        public void SaveItem(tb_services_balances_sums item)
        {
            item.date_post = DateTime.Now;
            context.tb_services_balances_sums.Add(item);

            context.SaveChanges();
        }
    }
}
