﻿using System.Collections.Generic;
using Easypay.Domain.Conctere;

namespace Easypay.Domain.Abstract
{
   public interface IRepository
    {
        IEnumerable<tb_services_balances> ProviderBalances { get; }
        IEnumerable<tb_services_balances_sums> BalanceSums { get; }
       void SaveItem(tb_services_balances_sums item);
    }
}
